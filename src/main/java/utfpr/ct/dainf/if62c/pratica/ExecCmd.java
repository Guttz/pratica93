/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Gustavo
 */

public class ExecCmd extends Thread{
    private final String cmd;
    Process processo;
    
    public ExecCmd(String cmd){
        this.cmd = cmd;
    }
    
    @Override
    public void run(){
        try{
            processo = Runtime.getRuntime().exec(cmd);
            synchronized(processo){
            processo.waitFor();    
            }
            
        } catch (Exception e){
            e.getLocalizedMessage();
        }
    }
    
    public void cancela(){
        if(processo == null)
            return;
        
        processo.destroy();
    }
    
    public boolean terminado(){
        if(processo == null)
            return true;
        
        return !(processo.isAlive());
    }
}
