
import java.io.BufferedReader;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Scanner;
import utfpr.ct.dainf.if62c.pratica.ExecCmd;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Gustavo
 */
public class Pratica93 {
    
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        String cmd;
        ArrayList<ExecCmd> Processos = new ArrayList<>();
        boolean processoNaoTerminado = false;
        boolean terminar = false;
        
        while(!terminar){
            System.out.println("comando>");
            cmd = scanner.next();
            
            if("fim".equals(cmd)){
                for(ExecCmd ProcessoAtual: Processos){
                    if(!ProcessoAtual.terminado())
                        processoNaoTerminado = true;
                }
                    
                    if(!processoNaoTerminado){
                        terminar = true;
                        continue;
                        }
                    
                    else{
                        System.out.println("Deseja terminar os processos? s/n");
                    
                    if(scanner.next().equals("s")){
                        for(ExecCmd ProcessoAtual: Processos) {
                            if(!ProcessoAtual.terminado())
                                ProcessoAtual.cancela();
                                terminar = true;
                                continue;
                        }
                    }
                    }
            }
            ExecCmd Processo = new ExecCmd(cmd);
            
            Processo.start();
            Processos.add(Processo);
        }
        
    }
}
